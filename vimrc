set tabstop=4
set shiftwidth=4
set expandtab
set nu

execute pathogen#infect()
syntax on
filetype plugin indent on

set t_Co=256
colorscheme zenburn

let g:neocomplete#enable_at_startup = 1